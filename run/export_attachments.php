<?php
$path = $argv[1];
$run_opts = $argv[2];

require_once('/etc/openbiomaps/system_vars.php.inc');
require_once($path.'local_vars.php.inc');
require_once($path.'includes/job_functions.php');

if (!$ID = PGconnectSQL(gisdb_user,gisdb_pass,gisdb_name,gisdb_host)) 
    die("Unsuccessful connect to GIS databases.\n");

require_once($path.'includes/postgres_functions.php');
require_once($path.'includes/cache_functions.php');
require_once($path.'includes/common_pg_funcs.php');
require_once($path.'includes/default_modules.php');
require_once($path.'includes/admin_pages/jobs.php');
require_once($path.'jobs/run/lib/export_attachments.php');

job_log('export_attachments started');

if (!class_exists('export_attachments')) {
    job_log("class export_attachments does not exists\n");
    exit();
}
if (!method_exists('export_attachments','run')) {
    job_log("method 'run' does not exists\n");
    exit();
}

    
#call_user_func(['export_attachments','init']);
call_user_func_array(['export_attachments','run'],[$run_opts]);

?>
