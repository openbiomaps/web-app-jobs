<?php
$path = $argv[1];
$run_opts = $argv[2];

require_once('/etc/openbiomaps/system_vars.php.inc');
require_once($path.'local_vars.php.inc');
require_once($path.'includes/job_functions.php');

if (!$ID = PGconnectSQL(biomapsdb_user,biomapsdb_pass,gisdb_name,gisdb_host)) 
    die("Unsuccessful connect to GIS database with biomaps.");

require_once($path.'includes/postgres_functions.php');
require_once($path.'includes/cache_functions.php');
require_once($path.'includes/common_pg_funcs.php');
require_once($path.'includes/default_modules.php');
require_once($path.'includes/admin_pages/jobs.php');
require_once($path.'jobs/run/lib/chirovox_rename.php');

job_log('chirovox_rename started');

if (!class_exists('chirovox_rename')) {
    job_log("class chirovox_rename does not exists\n");
    exit();
}
if (!method_exists('chirovox_rename','run')) {
    job_log("method 'run' does not exists\n");
    exit();
}

    
#call_user_func(['chirovox_rename','init']);
call_user_func_array(['chirovox_rename','run'],[$run_opts]);

?>
