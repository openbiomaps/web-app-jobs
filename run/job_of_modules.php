<?php
$path = $argv[1];
$arg = $argv[2];

putenv("OB_LIB_DIR={$path}includes/");

require_once('/etc/openbiomaps/system_vars.php.inc');
require_once($path.'local_vars.php.inc');
require_once($path.'includes/job_functions.php');

if (!$ID = PGconnectSQL(gisdb_user,gisdb_pass,gisdb_name,gisdb_host)) 
    die("Unsuccessful connect to GIS databases.\n");
if (!$GID = PGconnectSQL(biomapsdb_user,biomapsdb_pass,gisdb_name,gisdb_host)) 
        die("Unsuccessful connect to GIS database.");
if (!$BID = PGconnectSQL(biomapsdb_user,biomapsdb_pass,biomapsdb_name,biomapsdb_host))
    die("Unsuccesful connect to UI database.");

require_once($path.'includes/postgres_functions.php');
require_once($path.'includes/cache_functions.php');
require_once($path.'includes/common_pg_funcs.php');
require_once($path.'includes/modules_class.php');
require_once($path.'includes/default_modules.php');
require_once($path.'includes/admin_pages/jobs.php');
require_once($path.'jobs/run/lib/job_of_modules.php');

job_log('job_of_modules started');

if (!class_exists('job_of_modules')) {
    job_log("class job_of_modules does not exists\n");
    exit();
}
if (!method_exists('job_of_modules','run')) {
    job_log("method 'run' does not exists\n");
    exit();
}

    
call_user_func(['job_of_modules','run']);

?>
