<?php 
$path = $argv[1];
require_once('/etc/openbiomaps/system_vars.php.inc');
require_once($path.'/local_vars.php.inc');
require_once($path.'/includes/job_functions.php');

if (!$ID = PGconnectSQL(gisdb_user,gisdb_pass,gisdb_name,gisdb_host)) 
    die("Unsuccessful connect to GIS databases.\n");

require_once($path.'/includes/postgres_functions.php');
require_once($path.'/includes/default_modules.php');
require_once($path.'/includes/admin_pages/jobs.php');
require_once($path.'/jobs/run/lib/sql_daily.php');

job_log('sql_daily started');

if (!class_exists('sql_daily')) {
    job_log("class sql_daily does not exists\n");
    exit();
}
if (!method_exists('sql_daily','run')) {
    job_log("method 'run' does not exists\n");
    exit();
}

    
call_user_func(['sql_daily','run']);

?>
