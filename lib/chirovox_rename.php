<?php
class chirovox_rename extends job_module {

    public function __construct($mtable) {
        parent::__construct(__CLASS__,$mtable);
    }

    public function init($params,$pa) {
        debug('chirovox_rename initialized', __FILE__, __LINE__);
        return true;
    }

    static function run($run_opts) {
        job_log("running chirovox_rename module");
        global $ID;

        $params = parent::getJobParams(__CLASS__);

        if ($params) {
            //job_log($params);
            extract((array)$params);
        }

        $path = OB_ROOT.'projects/'.PROJECTTABLE.'/local/';
        if (!file_exists($path.'attached_files')) {
            job_log("Dir not exists: $path".'attached_files');
            return;
        }

        $cmd = sprintf('SELECT f.reference,chirovox,randtext
                    FROM system.files f
                    LEFT JOIN system.file_connect ON id=file_id
                    LEFT JOIN %1$s ON conid=obm_files_id
                    WHERE f.reference !~ \'[A-E][0-9]+_[A-Z]{10}\.(WAV|zc|wav|ZC)\'
                    AND project_table=\'%1$s\'',PROJECTTABLE);

        $res = pg_query($ID,$cmd);
        
        while ($row = pg_fetch_assoc($res)) {

            $file = $row['reference'];
            if (preg_match('/(.+)\.(WAV|zc|wav|ZC)$/',$file,$m)) {
                $new_filename = $row['chirovox'].'_'.$row['randtext'].'.'.strtoupper($m[2]);
                if (is_link($path."attached_files/$file")) {
                    $link_target = readlink($path."attached_files/$file");
                    link($link_target, $path."attached_files/$new_filename");
                } else {
                #    rename($path."attached_files/$file", $path."attached_files/$new_filename");
                    link($path."attached_files/$file", $path."attached_files/$new_filename");
                }
                //job_log($path."attached_files/$file", $path."attached_files/$new_filename");
                $cmd = sprintf('UPDATE system.files SET reference=\'%1$s\' WHERE reference=\'%2$s\' AND project_table=\'%3$s\'',$new_filename,$file,PROJECTTABLE );
                $u_res = pg_query($ID,$cmd);
                //job_log($cmd);
            }
        }

    }
}
?>
