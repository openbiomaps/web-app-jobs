<?php
class telepules_hozzarendeles extends job_module {

    public function __construct($mtable) {
        parent::__construct(__CLASS__,$mtable);
    }

    public function init($params,$pa) {
        debug('hnpi_telepules_hozzarendeles initialized', __FILE__, __LINE__);
        return true;
    }

    static function run() {
        job_log("running telepules_hozzarendeles module");

        global $ID;
        
        $params = parent::getJobParams(__CLASS__);
        
        if (!$params) {             
            job_log('job parametes missing: {"target_table":"","target_column":"","used_geom":"","source_geom":"","source_table":"","source_column":"",...}');             
            return;         
        }         
        extract((array)$params); 
        
        $T_table = $target_table;
        $T_column = $target_column;
        $T_geom = $used_geom;
        $S_column = $source_column;
        $S_geom = $source_geom;
        $S_table = $source_table;


        $cmd = "SELECT count(*) AS \"count\" FROM \"$T_table\" WHERE $T_column IS NULL OR $T_column=''";
        $res = pg_query($ID, $cmd);
        $rows = pg_fetch_assoc($res);
        job_log("Need update for ". $rows['count']." rows.");

        $cmd = "UPDATE $T_table SET $T_column = foo.$S_column FROM (SELECT $S_column,$S_geom FROM $S_table) foo WHERE st_within($T_geom,$S_geom) AND ($T_column IS NULL OR $T_column='')";
        job_log($cmd);


        job_log("Update started");

        $res = pg_query($ID, $cmd);
        job_log("Updated rows:". pg_affected_rows($res));

        exit;

        /* R Job execute */
        #$r = (defined('R_PATH')) ? constant('R_PATH') : '/usr/bin/Rscript';
        #exec("nohup $r --vanilla $filename $dirname >>$event_log 2>>$error_log &");
        
        /* Executable job file, e.g .pl, .sh, .. */
        #exec("nohup $filename $dirname >>$event_log 2>>$error_log &");
    }
}
?>
