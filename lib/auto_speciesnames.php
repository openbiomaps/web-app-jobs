<?php
class auto_speciesnames extends job_module {

    public function __construct($mtable) {
        parent::__construct(__CLASS__,$mtable);
    }

    public function init($params,$pa) {
        debug('auto_speciesnames initialized', __FILE__, __LINE__);
        return true;
    }

    static function run() {
        job_log("running auto_speciesnames module");
        global $ID;
        
        $langs = taxon_languages();
        job_log($langs);
        $source_lang = $langs['src'];
        unset($langs['src']);
        
        $cmd = [];
        foreach ($langs as $taxon_lang) {
            $cmd[] = "
                WITH rows AS (
                    SELECT DISTINCT f.$source_lang, foo.valid_word 
                    FROM dinpi f 
                    LEFT JOIN (
                        SELECT t.word as src_word, tt.word as valid_word 
                        FROM dinpi_taxon t 
                        LEFT JOIN dinpi_taxon tt ON t.taxon_id = tt.taxon_id AND tt.status = 'accepted'
                        WHERE tt.lang = '$taxon_lang' AND tt.word IS NOT NULL
                    ) foo ON f.$source_lang = foo.src_word 
                    WHERE f.$taxon_lang IS NULL AND foo.valid_word IS NOT NULL) 
                UPDATE dinpi ff SET $taxon_lang = rows.valid_word 
                FROM rows 
                WHERE rows.$source_lang = ff.$source_lang AND ff.$taxon_lang IS NULL;
            ";
        }
        debug($cmd[0]);
        #query($ID, $cmd);
        exit;
    }
}
?>
