<?php
class kaszalasi_bejelento extends job_module {

    var $months;
    var $mailfrom;
    var $def_mailto;
    var $def_title;
    var $subject_template;
    var $project;
    var $url;
    var $verbode;

    public function __construct($mtable) {
        parent::__construct(__CLASS__,$mtable);
    }

    public function init() {

        //debug('kaszalasi_bejelento initialized', __FILE__, __LINE__);
        /*
        CREATE TABLE {$table}_alert (
            id integer NOT NULL,
            bejelentes_id integer NOT NULL,
            row_biotika text,
            status character varying(6),
            datetime timestamp with time zone DEFAULT now(),
            row_id integer,
            row_datum date,
            row_faj character varying(64)
        );

        ALTER TABLE public.{$table}_alert OWNER TO {$table}_admin;
        COMMENT ON TABLE public.{$table}_alert IS 'Automatikus figyelmeztetések';
        COMMENT ON COLUMN public.{$table}_alert.bejelentes_id IS 'bejelentés azonosítója';
        COMMENT ON COLUMN public.{$table}_alert.row_biotika IS 'lekérdezett biotikai adatok';
        COMMENT ON COLUMN public.{$table}_alert.status IS 'státusz: új, olvasott, folyamatban,...';
        COMMENT ON COLUMN public.{$table}_alert.datetime IS 'automatikus időpont';
        CREATE SEQUENCE public.{$table}_alert_id_seq
            START WITH 1
            INCREMENT BY 1
            NO MINVALUE
            NO MAXVALUE
            CACHE 1;
        ALTER TABLE public.{$table}_alert_id_seq OWNER TO {$project}_admin;
        ALTER SEQUENCE public.{$table}_alert_id_seq OWNED BY public.{$table}_alert.id;
        ALTER TABLE ONLY public.{$table}_alert ALTER COLUMN id SET DEFAULT nextval('public.{$table}_alert_id_seq'::regclass);
        ALTER TABLE ONLY public.{$table}_alert
               ADD CONSTRAINT {$table}_alert_pkey PRIMARY KEY (id);
        ALTER TABLE ONLY public.{$table}_alert
               ADD CONSTRAINT bejelentesek_id FOREIGN KEY (bejelentes_id) REFERENCES public.{$table}_bejelentesek(id) ON UPDATE CASCADE ON DELETE CASCADE;
        GRANT SELECT ON TABLE public.{$table}_alert TO {$table}_user;
        
        CREATE TABLE {$table}_alert_people (
            email character varying(128) NOT NULL,
            geometry geometry NOT NULL,
            name character varying(128),
            CONSTRAINT enforce_dims_geometry CHECK ((st_ndims(geometry) = 2)),
            CONSTRAINT enforce_geotype_geometry CHECK ((geometrytype(geometry) = 'POLYGON'::text)),
            CONSTRAINT enforce_srid_geometry CHECK ((st_srid(geometry) = 4326))
        );
        ALTER TABLE public.knp_alert_people OWNER TO {$table}_admin;
        ALTER TABLE ONLY public.{$table}_alert_people
            ADD CONSTRAINT {$table}_alert_people_pkey PRIMARY KEY (email);

        -- kaszálási bejelentő tábla
        CREATE TABLE {$table}_bejelentesek (
            id integer NOT NULL,
            iktatoszam character varying(16),
            ugyfel character varying(64),
            cim character varying(255),
            telepules character varying(64),
            hrszkod character varying(16),
            alrkod character varying(2),
            blokkaz character varying(16),
            terulet numeric,
            terv_date date,
            bej_date date,
            n2000 boolean,
            mtet character varying(32),
            kultura character varying(32),
            megjegyzes text,
            dok text,
            dok_tipus character varying(32),
            felt_datum date DEFAULT now(),
            geometry geometry(Geometry,4326),
            kasz_tipus character varying(128),
            CONSTRAINT enforce_dims_geometry CHECK ((st_ndims(geometry) = 2)),
            CONSTRAINT enforce_geotype_geometry CHECK ((geometrytype(geometry) = 'POLYGON'::text)),
            CONSTRAINT enforce_srid_geometry CHECK ((st_srid(geometry) = 4326))
        );

        ALTER TABLE public.{$table}_bejelentesek OWNER TO {$table}_admin;
        COMMENT ON COLUMN public.{$table}_bejelentesek.iktatoszam IS 'iktatószám';
        COMMENT ON COLUMN public.{$table}_bejelentesek.ugyfel IS 'Név';
        COMMENT ON COLUMN public.{$table}_bejelentesek.cim IS 'cím';
        COMMENT ON COLUMN public.{$table}_bejelentesek.telepules IS 'település';
        COMMENT ON COLUMN public.{$table}_bejelentesek.hrszkod IS 'hrsz';
        COMMENT ON COLUMN public.{$table}_bejelentesek.alrkod IS 'alrészlet';
        COMMENT ON COLUMN public.{$table}_bejelentesek.blokkaz IS 'MEPAR blokk azonosító';
        COMMENT ON COLUMN public.{$table}_bejelentesek.terv_date IS 'kaszálás tervezett ideje';
        COMMENT ON COLUMN public.{$table}_bejelentesek.bej_date IS 'bejelentés dátuma';
        COMMENT ON COLUMN public.{$table}_bejelentesek.n2000 IS 'Natura 2000 bejelentés';
        COMMENT ON COLUMN public.{$table}_bejelentesek.mtet IS 'Valamelyik AKG miatt jelenti be.';
        COMMENT ON COLUMN public.{$table}_bejelentesek.kultura IS 'élőhelytípus (lehetne pl. lucerna is)';
        COMMENT ON COLUMN public.{$table}_bejelentesek.dok IS 'eltárolt dokumentum';
        COMMENT ON COLUMN public.{$table}_bejelentesek.dok_tipus IS 'dokumentum típusa';
        COMMENT ON COLUMN public.{$table}_bejelentesek.felt_datum IS 'adatfeltöltés dátuma';
        COMMENT ON COLUMN public.{$table}_bejelentesek.geometry IS 'érintett terület térbelisége';
        COMMENT ON COLUMN public.{$table}_bejelentesek.kasz_tipus IS 'Bejelentés típusa (korai kaszáls, N2000 gyep, MTÉT, stb.)';
        
        CREATE SEQUENCE public.{$table}_bejelentesek_id_seq
            START WITH 1
            INCREMENT BY 1
            NO MINVALUE
            NO MAXVALUE
            CACHE 1;

        ALTER TABLE public.{$table}_bejelentesek_id_seq OWNER TO {$table}_admin;

        ALTER SEQUENCE public.{$table}_bejelentesek_id_seq OWNED BY public.{$table}_bejelentesek.id;

        ALTER TABLE ONLY public.{$table}_bejelentesek ALTER COLUMN id SET DEFAULT nextval('public.{$table}_bejelentesek_id_seq'::regclass);

        ALTER TABLE ONLY public.{$table}_bejelentesek
            ADD CONSTRAINT {$table}_bejelentesek_pkey PRIMARY KEY (id);

        CREATE TRIGGER alert AFTER INSERT ON public.{$table}_bejelentesek FOR EACH ROW EXECUTE PROCEDURE public.{$table}_notify();

        ALTER TABLE public.{$table}_bejelentesek DISABLE TRIGGER alert;

        CREATE TRIGGER bejelentes_trigger AFTER INSERT OR UPDATE ON public.{$table}_bejelentesek FOR EACH ROW EXECUTE PROCEDURE public.bejelentes_trigger();

        GRANT SELECT ON TABLE public.{$table}_bejelentesek TO {$table}_user;

        CREATE OR REPLACE FUNCTION bejelentes_trigger() 
        RETURNS trigger 
        AS 
        $bejelentes_trigger$
        BEGIN
            IF tg_op = 'INSERT' OR tg_op = 'UPDATE'  THEN
                 INSERT INTO {$table}_alert (bejelentes_id,status,row_id,row_datum,row_faj,row_biotika)
                 SELECT new.id,'uj',obm_id,datum,faj,
                    row_to_json((SELECT d FROM (SELECT egyed,adatkozlo,gyujto,gyujtesi_mod,felmeres_mod) d)) AS data
                 FROM {$table} WHERE ST_Contains(new.geometry,obm_geometry);
                 RETURN new;
             END IF;
        END
        $bejelentes_trigger$
        LANGUAGE plpgsql;

        CREATE OR REPLACE FUNCTION knp_notify() 
        RETURNS trigger 
        AS 
        $notify$
        BEGIN
            PERFORM pg_notify('bejelentes', row_to_json(NEW)::text);
            RETURN NEW;
        END
        $notify$
        LANGUAGE plpgsql;

         */

        return true;
    }

    static function run() {
        global $ID;

        job_log("running kaszalasi_bejelento module");

        $params = parent::getJobParams(__CLASS__);
        if (!$params) {
            job_log('job parametes missing');
            return;
        }

        extract((array)$params);

        // Read variables
        //$date = date('Y-01-01');
        $months = isset($data_period_months) ? $data_period_months : 36; // how oldest data will be queried
        $mailfrom = isset($mail_from) ? $mail_from : ""; //default central email address
        $def_mailto = isset($def_mailto) ? $def_mailto : ""; //default central email address
        $def_title = isset($def_title) ? $def_title : "Kedves kolléga!";
        $subject_template = isset($subject_template) ? $subject_template : "Új kaszálási bejelentő";
        $project = isset($project) ? $project : PROJECTTABLE;
        $url = isset($url) ? $url : "";
        $verbode = isset($verbose) ? $verbose : 1;


        #
        # query new alerts' data counts
        $cmd = sprintf('SELECT DISTINCT bejelentes_id,iktatoszam,ugyfel,cim,telepules,hrszkod,alrkod,blokkaz,terulet,terv_date,bej_date,n2000,mtet, 
            kultura,megjegyzes,dok,dok_tipus,felt_datum,kasz_tipus,email,name
                        FROM "%1$s_alert" a LEFT JOIN "%1$s_bejelentesek" b ON (a.bejelentes_id=b.id)
                            LEFT JOIN %1$s_alert_people ap ON (st_intersects(ap.geometry,b.geometry))
                        WHERE status=\'uj\'',$project);

        $result = pg_query($ID,$cmd);
        $alert_name = array();
        $alert_mail = array();
        $bejelentes_id = array();
        $bejelentes_data = array();
        while($r = pg_fetch_assoc($result)) {
            $alert_name[] = $r['name'];
            $alert_mail[] = $r['email'];
            $bejelentes_id[] = $r['bejelentes_id'];
            $bejelentes_data[] = array($r['iktatoszam'],$r['ugyfel'],$r['cim'],$r['telepules'],$r['hrszkod'],$r['alrkod'],$r['blokkaz'],$r['terulet'],$r['terv_date'],$r['bej_date'],$r['n2000'],$r['mtet'],$r['kultura'],$r['megjegyzes'],$r['dok'],$r['dok_tipus'],$r['felt_datum'],$r['kasz_tipus']);
        }
        #$subject = sprintf("%d %s [%s]",count($bejelentes_id),$subject_template,'DHTE');

        if (count($bejelentes_id) == 0)
            exit;
                                                                                                                                        
        #$message = "<!DOCTYPE html><head><meta http-equiv='Content-Type' content='text/html; charset=utf-8' /><style>h2{color:indianred}h3{color:mediumslateblue}ul{list-style-type:none;color:gray}</style></head>";
        #$message .= "Bejelentések: ";
        #foreach ($bejelentes_id as $bid) {
        #    $message .= sprintf('<a href="#b%1$d">%1$s</a> ',$bid);
        #}
        #$message .= "<hr>";

        $n = 0;
        $mail_sent = 0;   
        $mail_array = array();

        # 522 526
        foreach ($bejelentes_id as $bid) {

            $subject = sprintf("%s [%d - %s]",$subject_template,$bid,'DHTE');

            $message = "<!DOCTYPE html><head><meta http-equiv='Content-Type' content='text/html; charset=utf-8' /><style>h2{color:indianred}h3{color:mediumslateblue}ul{list-style-type:none;color:gray}</style></head>";
            $message .= "<br>";
            $message .= "<h2>Bejelentés adatok</h2><h3><a name='b$bid'>Bejelentési azonosító: $bid</a></h3><ul>";
            $message .= "<li>".implode('</li><li>',$bejelentes_data[$n])."</li>";

            # query alerts
            $cmd = sprintf("SELECT id,bejelentes_id,row_biotika,status,datetime,row_id,row_datum,row_faj FROM \"%s_alert\" WHERE status = 'uj' AND row_datum > NOW() - INTERVAL '%d month' AND bejelentes_id=%d ORDER by row_faj",$project,$months,$bid); 

            $result = pg_query($ID,$cmd);
            if (pg_num_rows($result) == 0) {
                $message .= "<h3>Nincsenek érintett biotikai adatok a területről</h3>";
            } else {
                $message .= "<h3>Biotikai adatok</h3>";
            }
            # 522-1 522-2 522-3 522-4
            while($row=pg_fetch_assoc($result)) {
                $message .= sprintf("<a href='http://%s/index.php?data&id=%d'>%s [%s]</a>: <ul><li>%s</li></ul>",$url,$row['row_id'],$row['row_faj'],$row['row_datum'],implode("</li><li>",json_decode($row['row_biotika'],true)));
            }

            $message .= "</ul></html>";

            // sending mail
            if ($verbode)
                print "Sending mails!\n";

            if ($alert_mail[$n]!='') $mailto = $alert_mail[$n];
            else $mailto = $def_mailto;
            if (preg_match('/,/',$mailto)) {
                $mailto = preg_split('/,/',$mailto);
            }

            if ($alert_name[$n]!='') $title = "Kedves ".$alert_name[$n]."!";
            else $title = $def_title;

            $message = "<h1>$title</h1>$message";
            $m = mail_to($mailto,$subject,$mailfrom,$mailfrom,$message,"multipart"); 
            #$mail_array['mailto'] = $mailto; 
            #$mail_array['subject'] = $subject;
            #$mail_array['mailfrom'] = $mailfrom;
            #$mail_array['title'] = $title;
            #$mail_array['message'] = $message;
            #$m = remote_job_call('mail',$mail_array,'ssh','openbiomaps.org','banm');

            if ($verbode) {
                print "Mail status: $m\n";
            }
            if ($m == 2) {
                // update alert table if mail sent
                $mail_sent++;
                
                if ($verbode)
                    print "Update alert statuses!\n";
                $cmd = sprintf("UPDATE \"%s_alert\" SET status='sent' WHERE bejelentes_id = %d",$project,$bid);
                $result = pg_query($ID,$cmd);
            
            } else {
                if ($verbode) {
                    print "Mail sending failed.\n";
                    print "Save alert report to file.\n";
                }

            }
            $n++;
        }



	   	// Ez mi????
        if ($mail_sent < count($bejelentes_id)) {
            $filename = sprintf("data_%s.html",implode('-',$bejelentes_id));
            $fp = fopen($filename, 'w');
            fwrite($fp, "<h1>$title</h1>".$message);
            fclose($fp);
        }

    }
}
?>
