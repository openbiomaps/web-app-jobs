<?php
class superspecies_autonames extends job_module {

    public function __construct($mtable) {
        parent::__construct(__CLASS__,$mtable);
    }

    public function init($params,$pa) {
        //debug('auto_speciesnames initialized', __FILE__, __LINE__);
        return true;
    }

    static function run() {
        job_log("running superspecies_autonames job");
        global $ID;

        $params = parent::getJobParams(__CLASS__);
        if (!$params) {
            job_log('job parametes missing: {"db_name":"","species_name_col":"","auto_scientific_name_col":"","auto_nationalname_1":"",...}');
            return;
        }

        extract((array)$params);

        $biotika_db = $db_name;
        $sp_namecol = $species_name_col;
        $sci_auto = $auto_scientific_name_col;
        $nat1_auto = $auto_nationalname_1;

        $cmd = "ALTER TABLE public.$biotika_db DISABLE TRIGGER history_update_$biotika_db;
	    	   	ALTER TABLE public.$biotika_db DISABLE TRIGGER rules_$biotika_db;
	    	    ALTER TABLE public.$biotika_db DISABLE TRIGGER taxon_db_update;
	    	    ALTER TABLE public.$biotika_db DISABLE TRIGGER taxon_update_$biotika_db;
	    	    ALTER TABLE public.$biotika_db DISABLE TRIGGER update_scientific_name;";
	    $res = pg_query($ID, $cmd);
        job_log("Triggers turned off". pg_num_rows($res));

        // teljes egyezés update
        $cmd = "UPDATE $biotika_db SET $sci_auto=species_name, $nat1_auto=magyarnev
                    FROM (
                        SELECT species_name, a[1] AS magyarnev FROM (
                            SELECT species_name, string_to_array(hun_magyarnev,',') AS a FROM shared.superspecies) bar
                    ) foo 
                WHERE foo.species_name=$sp_namecol AND $sci_auto IS NULL;";

        $res = pg_query($ID, $cmd);
        job_log("Full match:". pg_affected_rows($res));

        // regexpek kis-nagybetűre
        $cmd = "WITH foo(species_name, magyarnev) AS (
                    SELECT species_name, array_agg(regexp_replace(col, '\s+', '', 'g')) as magyarnev
                      FROM shared.superspecies, unnest(string_to_array(hun_magyarnev,',')) un(col)
                    GROUP BY species_name
                )
                UPDATE $biotika_db SET $sci_auto=species_name, $nat1_auto=magyarnev[1]
                FROM foo
                WHERE lower($sp_namecol) = ANY(magyarnev) AND $sci_auto IS NULL;";

        $res = pg_query($ID, $cmd);
        job_log("Ignore-case full match:". pg_affected_rows($res));
        
        // regexpek spaceekre
        $cmd = "WITH foo(species_name, magyarnev) AS (
                  SELECT species_name, array_agg(regexp_replace(col, '\s+', '', 'g')) as magyarnev
                    FROM shared.superspecies, unnest(string_to_array(hun_magyarnev,',')) un(col)
                  GROUP BY species_name
                )
                UPDATE $biotika_db SET $sci_auto=species_name, $nat1_auto=magyarnev[1]
                FROM foo
                WHERE lower(REGEXP_REPLACE($sp_namecol,'\s+','','g')) = ANY(magyarnev) AND $sci_auto IS NULL";

        $res = pg_query($ID, $cmd);
        job_log("Regexp for whitespaces:". pg_affected_rows($res));

        // common mispells
        $cmd = "UPDATE $biotika_db SET $sci_auto=species_name, $nat1_auto=magyarnev
            FROM (
                SELECT species_name, a[1] AS magyarnev, b[1] as mispell FROM (                
                    SELECT species_name, string_to_array(hun_magyarnev,',') AS a, string_to_array(common_mispells,',') AS b 
                    FROM shared.superspecies) bar
                ) foo
            WHERE mispell=$sp_namecol AND $sci_auto IS NULL";

        $res = pg_query($ID, $cmd);
        job_log("Common mispells-1:". pg_affected_rows($res));

        // common mispells - stupid solution for multiple mispells...
        $cmd = "UPDATE $biotika_db SET $sci_auto=species_name, $nat1_auto=magyarnev
            FROM (
                SELECT species_name, a[1] AS magyarnev, b[2] as mispell FROM (                
                    SELECT species_name, string_to_array(hun_magyarnev,',') AS a, string_to_array(common_mispells,',') AS b 
                    FROM shared.superspecies) bar
                ) foo
            WHERE mispell=$sp_namecol AND $sci_auto IS NULL";

        $res = pg_query($ID, $cmd);
        job_log("Common mispells-2:". pg_affected_rows($res));


        // similarity based update: scientific name
        $cmd = "SET pg_trgm.similarity_threshold = 0.7;
            UPDATE $biotika_db SET $sci_auto=species_name, $nat1_auto=magyarnev[1] FROM (
	        SELECT $sp_namecol,species_name,string_to_array(hun_magyarnev,',') as magyarnev, similarity($sp_namecol,species_name) as sim 
		    FROM $biotika_db JOIN shared.superspecies ON (species_name <> $sp_namecol AND species_name % $sp_namecol)
	        GROUP BY $sp_namecol, species_name, superspecies.hun_magyarnev, sim
	        ORDER BY sim DESC) foo
            WHERE $sci_auto IS NULL AND $biotika_db.$sp_namecol=foo.$sp_namecol";

        $res = pg_query($ID, $cmd);
        job_log("Similarity based updates for scientific name:". pg_affected_rows($res));

        // similarity based update: national name
        $cmd = "SET pg_trgm.similarity_threshold = 0.7;
            UPDATE $biotika_db SET $sci_auto=species_name, $nat1_auto=magyarnev[1] FROM (
                    SELECT $sp_namecol, species_name, magyarnev, similarity($sp_namecol,magyarnev[1]) as sim 
                            FROM $biotika_db JOIN (
                          SELECT species_name, string_to_array(hun_magyarnev,',') AS magyarnev FROM shared.superspecies
                       ) superspecies ON $sp_namecol <> ANY(magyarnev) AND $sp_namecol % ANY(magyarnev)
                    GROUP BY $sp_namecol,magyarnev,species_name,sim
                    ORDER BY sim DESC) foo
            WHERE $sci_auto IS NULL AND $biotika_db.$sp_namecol=foo.$sp_namecol;";

        $res = pg_query($ID, $cmd);
        job_log("Similarity based updates for national name:". pg_affected_rows($res));


        $cmd = "SELECT obm_id FROM $biotika_db WHERE $sci_auto IS NULL ORDER BY $sp_namecol";
        $res = pg_query($ID, $cmd);
        job_log("Not-validated records remaind in the table:". pg_num_rows($res));

        $cmd = "ALTER TABLE public.$biotika_db ENABLE TRIGGER history_update_$biotika_db;
	    	    ALTER TABLE public.$biotika_db ENABLE TRIGGER rules_$biotika_db;
	    	    ALTER TABLE public.$biotika_db ENABLE TRIGGER taxon_db_update;
	    	    ALTER TABLE public.$biotika_db ENABLE TRIGGER taxon_update_$biotika_db;
	    	    ALTER TABLE public.$biotika_db ENABLE TRIGGER update_scientific_name;";
        $res = pg_query($ID, $cmd);
        job_log("Triggers turned on". pg_num_rows($res));

        exit;
    }
}
?>
