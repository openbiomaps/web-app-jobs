<?php
class export_data extends job_module {

    public function __construct($mtable) {
        parent::__construct(__CLASS__,$mtable);
    }

    public function init($params,$pa) {
        debug('export_data initialized', __FILE__, __LINE__);
        return true;
    }


    static function run($run_opts = null) {
        $ro_b64dec = base64_decode($run_opts[0]);
        if (base64_encode($ro_b64dec) === $run_opts[0]) {
            $run_opts = json_decode(base64_decode($run_opts[0]), true);
            session_id($run_opts['session']['token']['session_id']);
            session_start();
            $_SESSION = $run_opts['session'];
            
            $de_id = (int)$run_opts['data_export_id'];
            $de = new DataExport($de_id);
            $de->set('status', 'running');
            $de->save();
            
            $status = $de->create_tmpfile($run_opts);
            
            $de->send_notification();
            
        }
        else {
            // TODO: 1. lejárt exportok törlése
            // TODO: 2. le nem töltött, lejáróban lévő export emlékeztető email
        }
    }
}
?>
