<?php
class intersect_data extends job_module {

    public function __construct($mtable) {
        parent::__construct(__CLASS__,$mtable);
    }

    public function init($params, $pa) {
        debug('intersect_data initialized', __FILE__, __LINE__);
        return true;
    }

    public function get_results() {
        global $ID;

        $params = parent::getJobParams(__CLASS__);
        $cmdpart = [];
        $tblHeader = ['total rows'];
        foreach ($params as $col => $opts) {
            $cmdpart[] = "count(CASE WHEN $col IS NOT NULL THEN 1 END) AS $col";
            $tblHeader[] = $col;
        }
        $cmd = sprintf("SELECT count(*) AS total, %s FROM %s_qgrids;", implode(', ', $cmdpart), PROJECTTABLE);

        if (!$res = query($ID,$cmd))
            return 'ERROR: query error';


        $results = pg_fetch_assoc($res[0]);

        $t = [$results['total']];
        $tbl = new createTable();
        $tbl->def(['tid'=>__CLASS__.'-results-table','tclass'=>'resultstable']);
        $tbl->addHeader($tblHeader);

        foreach ($params as $col => $opts) {
            $t[] = $results[$col]. ' ('. round($results[$col] * 100 / $t[0],2) .'%)';
        }

        $tbl->addRows($t);

        return $tbl->printOut();
    }

    static function run() {
        global $ID;
        
        $params = parent::getJobParams(__CLASS__);

        if (isset($params->multitable)) {
            $tables = $params->tables;
        }
        else {
            $tables = [PROJECTTABLE => $params];
        }

        if (isset($params->limit)) {
            if ($params->limit === 0) {
                $limit = "";
            } 
            else {
                $limit = "LIMIT " . (int)$params->limit;
            }
        }
        else {
            $limit = "LIMIT 1000";
        }

        foreach ($tables as $data_table => $columns) {
            $start = microtime(true);
            $cmd = [];
            
            foreach ($columns as $col => $opts) {

                if (isset($opts->limit)) {
                    $limit = "LIMIT " . (int)$opts->limit;
                }

                if (isset($opts->custom_query) && $opts->custom_query === true) {
                    $method = "intersect_with_$col";
                    if (method_exists(__CLASS__,$method)) {
                        job_log($method);
                        $cmd[] = self::$method($limit);
                    }
                    continue;
                }

                $filter = '1 = 1';
                $schema = $opts->schema ?? 'public';

                if (isset($opts->filterColumn) && isset($opts->filterValue)) {
                    $relation = $opts->filterRelation ?? null;
                    $filter = prepareFilter($opts->filterColumn, $opts->filterValue, $relation, 'g');
                }

                $transform = (isset($opts->srid))
                    ? sprintf("ST_Transform(rqg.centroid,%s)", $opts->srid)
                    : "rqg.centroid";

                $val = $opts->valueColumn;
                $groupBy = "";
                if (isset($opts->multipleMatch) && $opts->multipleMatch === 'concat') {
                    $val = sprintf("string_agg(g.%s, ', ')", $val);
                    $groupBy = sprintf("GROUP BY rqg.data_table, rqg.row_id");
                } 

                $cmd[] = sprintf('WITH rows AS ( SELECT rqg.data_table, rqg.row_id, %2$s as val FROM %9$s_qgrids rqg LEFT JOIN %3$s.%4$s g ON ST_Intersects(%6$s,%7$s) WHERE rqg.%1$s IS NULL AND %5$s %10$s ORDER BY row_id DESC %8$s) UPDATE %9$s_qgrids qg SET %1$s = rows.val FROM rows WHERE rows.data_table = qg.data_table AND rows.row_id = qg.row_id;',
                    $col,
                    $val,
                    $schema,
                    $opts->table,
                    $filter,
                    $transform,
                    $opts->geomColumn,
                    $limit,
                    $data_table,
                    $groupBy
                );
                if ($opts->valueColumn !== 'geometry') {
                    $cmd[] = sprintf('WITH rows AS ( SELECT rqg.data_table, rqg.row_id FROM %9$s_qgrids rqg WHERE rqg.%1$s IS NULL AND NOT EXISTS (SELECT 1 FROM %3$s.%4$s g WHERE ST_Intersects(%6$s,%7$s) AND %5$s) ORDER BY row_id DESC %8$s) UPDATE %9$s_qgrids qg SET %1$s = \'NA\' FROM rows WHERE rows.data_table = qg.data_table AND rows.row_id = qg.row_id;', 
                        $col,
                        $opts->valueColumn,
                        $schema,
                        $opts->table,
                        $filter,
                        $transform,
                        $opts->geomColumn,
                        $limit,
                        $data_table
                    );
                }
            }
            query($ID, $cmd);
            $duration = round(microtime(true) - $start, 2);
            $duration = ($duration >= 60) ? floor($duration / 60) . " perc " . ($duration % 60) . " mp" : "$duration mp";
            job_log(__CLASS__. ": $data_table: $duration");
        }
        $start = microtime(true);
        $cmd = sprintf("VACUUM %s_qgrids;", PROJECTTABLE);
        pg_query($ID, $cmd);
        $duration = round(microtime(true) - $start, 2);
        job_log(__CLASS__ . ": Vacuum: $duration mp");
        
    }
}
?>
