<?php
class hunviphab_tracklogs extends job_module {

    public function __construct($mtable) {
        parent::__construct(__CLASS__,$mtable);
    }

    public function init($params,$pa) {
        debug('observation_lists initialized', __FILE__, __LINE__);
        return true;
    }

    static function description() { 
        $d = array();
        $d[] = "This splits tracklogs to tracklog points and producing a long table which contains
            the observer, datetime, session_id (if applicable), form_id, tracklog_id and the tracklog point geometry";
        $d[] = "Currently it has no arguments, it is not a generalized job function yet";
        return $d;
    }

    static function run() {
        global $ID;
        
        $params = parent::getJobParams(__CLASS__);
        if (!$params) {
            job_log('job parametes missing');
            return;
        }

        foreach ($params as $table => $options) {
            extract((array)$options);
        }

        # Processing old hunviphab_tracklogs table
        # 2021-03-27 - 2021-07-27
        # It is not changing any more, so we don't need to update 
        # Do Not Run
        $cmd = "
        DROP TABLE IF EXISTS temporary_tables.hunviphab_tracklogs_processed_tmp1;
        CREATE TABLE  temporary_tables.hunviphab_tracklogs_processed_tmp1 AS
           SELECT user_id,trackname,tracklog_id,j->'properties'->'timestamp' AS timestamp,j->'geometry'->'coordinates'->>0 AS lon,j->'geometry'->'coordinates'->>1 AS lat 
           FROM (
               SELECT user_id,trackname,tracklog_id,json_array_elements(tracklog_geom -> 'features') as j 
               FROM hunviphab_tracklogs
           ) foo;";

        # # There are ~ 33.000 records with stange tracklogs with an other type of structure
        # Do Not Run
        $cmd = "
        UPDATE temporary_tables.hunviphab_tracklogs_processed_tmp1 SET timestamp = foo.timestamp
        FROM (
        SELECT user_id, trackname,tracklog_id, j->'properties'->'time' AS timestamp
        FROM (
           SELECT user_id,trackname,tracklog_id,json_array_elements(tracklog_geom -> 'features') as j 
           FROM hunviphab_tracklogs ) foo 
        WHERE (j->'properties'->'timestamp') IS null
        ) foo
        WHERE hunviphab_tracklogs_processed_tmp1.timestamp IS NULL AND hunviphab_tracklogs_processed_tmp1.trackname=foo.trackname AND
        hunviphab_tracklogs_processed_tmp1.user_id=foo.user_id";

        # Processing new system.tracklogs table
        # 2021-04-17 - 
        # Very heavy query, would better to add new rows instead regenerate the all
        # Run
        job_log('Processing system.tracklogs');
        $cmd = "
        DROP TABLE IF EXISTS  temporary_tables.hunviphab_tracklogs_processed_tmp2;
        CREATE TABLE temporary_tables.hunviphab_tracklogs_processed_tmp2 AS
            SELECT user_id,trackname,tracklog_id,j->'properties'->'timestamp' AS timestamp,j->'geometry'->'coordinates'->>0 AS lon,j->'geometry'->'coordinates'->>1 AS lat 
            FROM (
                SELECT user_id,trackname,tracklog_id,json_array_elements(tracklog_geom -> 'features') as j 
                FROM system.tracklogs s 
                -- Jó lenne valami prefilter, hogy insteret lehessen csinálni, de optimalizálva, ez semmit sem segít.
                --LEFT JOIN temporary_tables.hunviphab_tracklogs_processed_tmp2 t ON (t.tracklog_id!=s.tracklog_id)
                WHERE project='hunviphab' ) foo;";
        if ( query($ID, $cmd) ) {
            job_log('Done');
        } else {
        	job_log(pg_last_error($ID));
        }

        # Don't need to drop, just clean if needed
        # Do not run
        $cmd = "DROP TABLE IF EXISTS  public.hunviphab_tracklogs_processed;
        CREATE TABLE public.hunviphab_tracklogs_processed (
            obm_id integer DEFAULT nextval('public.hunviphab_tracklogs_processed_obm_id_seq'::regclass) NOT NULL,
            obm_geometry public.geometry,
            obm_uploading_id integer,
            obm_validation numeric,
            obm_comments text[],
            obm_modifier_id integer,
            obm_files_id character varying(32),
            eszlelo text,
            datetime timestamp without time zone,
            session text DEFAULT 'nem'::text,
            urlap text,
            eszlelo_neve text,
            tracklog_id text,
            CONSTRAINT enforce_dims_obm_geometry CHECK ((public.st_ndims(obm_geometry) = 2)),
            CONSTRAINT enforce_srid_obm_geometry CHECK ((public.st_srid(obm_geometry) = 4326))
        );
        ALTER TABLE public.hunviphab_tracklogs_processed OWNER TO hunviphab_admin;
        ALTER TABLE ONLY public.hunviphab_tracklogs_processed
            ADD CONSTRAINT hunviphab_tracklogs_processed_pkey PRIMARY KEY (obm_id);
        CREATE INDEX hunviphab_tracklogs_processed_datetime_idx ON public.hunviphab_tracklogs_processed USING btree (datetime);
        CREATE INDEX hunviphab_tracklogs_processed_geometry_idx ON public.hunviphab_tracklogs_processed USING gist (obm_geometry);
        GRANT SELECT ON TABLE public.hunviphab_tracklogs_processed TO hunviphab_user;";


        # SESSION TRACKLOGS
        # Very heavy query, would better to processing the new uploads only
        # Run
        job_log('Processing Session tracklogs');
        $cmd = "DROP TABLE IF EXISTS  temporary_tables.hunviphab_session_tracks;
        CREATE TABLE  temporary_tables.hunviphab_session_tracks AS 
         SELECT form_id, uploader_id, uploader_name, uploading_date, metadata->'observation_list_id' #>> '{}' AS session_id, metadata->'observation_list_start' #>> '{}' AS session_start, metadata->'observation_list_end' #>> '{}' as session_end, metadata->'measurements_num' #>> '{}' AS mesurments_num,json_array_elements(metadata->'observation_list_track_log') as tracklog
         FROM system.uploadings 
         WHERE project_table='hunviphab' AND metadata->'observation_list_track_log' IS NOT NULL;

        ALTER TABLE temporary_tables.hunviphab_session_tracks ADD column point_timestamp text;
        ALTER TABLE temporary_tables.hunviphab_session_tracks ADD column point text;

        UPDATE temporary_tables.hunviphab_session_tracks SET point_timestamp = tracklog->'time_stamp' #>> '{}';
        UPDATE temporary_tables.hunviphab_session_tracks SET point = tracklog->'point' #>> '{}';";
        if ( query($ID, $cmd) ) {
            job_log('Done');
        } else {
        	job_log(pg_last_error($ID));
        }

        # Do not run
        $cmd = "SELECT st_MakeLine(array_agg(g)) 
        FROM (
         SELECT st_PointFromText(point,4326) AS g FROM hunviphab_session_tracks WHERE session_id='2935f88a-bbd3-458b-a0e7-456b4c70e57e'  ORDER BY point_timestamp
        ) foo;";

        # Do not run
        $cmd = "DROP TABLE IF EXISTS  temporary_tables.hunviphab_session_track_lines;
        CREATE TABLE temporary_tables.hunviphab_session_track_lines AS
         SELECT st_MakeLine(array_agg(g)), session_id, form_id, uploader_id, uploader_name, uploading_date, session_start, session_end, mesurments_num
         FROM (
          SELECT st_PointFromText(point,4326) AS g, session_id, form_id, uploader_id, uploader_name, uploading_date, session_start, session_end, mesurments_num  
          FROM hunviphab_session_tracks 
          ORDER BY point_timestamp ) foo
         GROUP BY session_id, form_id, uploader_id, uploader_name, uploading_date, session_start, session_end, mesurments_num;";

        # Filling tracklogs_processed
        # old tracks
        # Run
        job_log('Delete from hunviphab_tracklogs_processed');
        $cmd = "DELETE FROM public.hunviphab_tracklogs_processed";
        if ( query($ID, $cmd) ) {
            job_log('Done');
        } else {
        	job_log(pg_last_error($ID));
        }

        job_log('Adding old tracklogs to hunviphab_tracklogs_processed - I');
        $cmd = "INSERT INTO  public.hunviphab_tracklogs_processed (eszlelo,obm_geometry,datetime,tracklog_id)
             SELECT user_id, st_setsrid(st_makepoint(lon::numeric,lat::numeric),4326), 
                  to_timestamp((timestamp::text)::numeric/1000),tracklog_id FROM  temporary_tables.hunviphab_tracklogs_processed_tmp1
            WHERE timestamp::text ~ '^[0-9\.]+$'";
        if ( query($ID, $cmd) ) {
            job_log('Done');
        } else {
        	job_log(pg_last_error($ID));
        }

        job_log('Adding old tracklogs to hunviphab_tracklogs_processed - II.');
        #-- there have been ~33.000 records with text timestamp
        $cmd = "INSERT INTO  public.hunviphab_tracklogs_processed (eszlelo,obm_geometry,datetime,tracklog_id)
             SELECT user_id, st_setsrid(st_makepoint(lon::numeric,lat::numeric),4326), 
                  timestamp::text::timestamp,tracklog_id FROM  temporary_tables.hunviphab_tracklogs_processed_tmp1
            WHERE timestamp::text !~ '^[0-9\.]+$'";
        if ( query($ID, $cmd) ) {
            job_log('Done');
        } else {
        	job_log(pg_last_error($ID));
        }
            
        # Recent tracks
        # Run
        job_log('Adding recent tracks to hunviphab_tracklogs_processed');
        $cmd = "INSERT INTO public.hunviphab_tracklogs_processed (eszlelo,obm_geometry,datetime,tracklog_id)
             SELECT user_id, st_setsrid(st_makepoint(lon::numeric,lat::numeric),4326), 
                  to_timestamp((timestamp::text)::numeric/1000),tracklog_id FROM temporary_tables.hunviphab_tracklogs_processed_tmp2
             WHERE timestamp::text ~ '^[0-9\.]+$'";
        if ( query($ID, $cmd) ) {
            job_log('Done');
        } else {
        	job_log(pg_last_error($ID));
        }


        # Session tracks
        # Run
        job_log('Adding session tracks to hunviphab_tracklogs_processed');
        $cmd = "INSERT INTO public.hunviphab_tracklogs_processed (datetime,obm_geometry,eszlelo,eszlelo_neve,session,urlap,tracklog_id)
         SELECT to_timestamp(point_timestamp::numeric/1000),st_PointFromText(point,4326),uploader_id,uploader_name,'igen',form_id,session_id 
         FROM  temporary_tables.hunviphab_session_tracks";
        if ( query($ID, $cmd) ) {
            job_log('Done');
        }

        # Észlelő nevek beírása
        # Run
        job_log('Updating observer names');
        $cmd = "UPDATE public.hunviphab_tracklogs_processed SET eszlelo_neve=foo.username 
        FROM (
         SELECT username,id::text FROM public.users WHERE project_table='hunviphab'
        ) foo WHERE eszlelo=id AND eszlelo_neve IS NULL";
        if ( query($ID, $cmd) ) {
            job_log('done');
        }
    }
}
