<?php
class observation_lists_without_temp extends job_module {

    public function __construct($mtable) {
        parent::__construct(__CLASS__,$mtable);
    }

    public function init($params,$pa) {
        debug('observation_lists_without_temp initialized', __FILE__, __LINE__);
        return true;
    }

    static function run() {
        global $ID;

        /*  Params
            {
                "your-project-table-name": {
                    "list_start_column": "session_start",
                    "list_end_column": "session_end",
                    "list_duration_column": "session_duration",
                    "list_id_column": "session_id",
                    "only_time": false,
                    "time_as_int": false
                }
            }
         *
         * CREATE TABLE %s.%s_observation_sessions (
            sids text,
            uploading_id integer,
            uploader_id integer,
            session_start timestamp without time zone,
            session_end timestamp without time zone,
            nullrecord boolean
            );
            ALTER TABLE %s ADD COLUMN 
                    session_start timestamp without time zone,
                    session_end timestamp without time zone,
                    session_id text,
                    session_duration time without time zone;
         *
         * */

        $params = parent::getJobParams(__CLASS__);
        if (!$params) {
            job_log('job parametes missing');
            return;
        }

        foreach ($params as $table => $options) {
            extract((array)$options);
            

            //get the unprocessed records
            $lists = self::get_unprocessed_lists( );
            if ($lists === 'error') {
                job_log('observation_lists failed 2');
                return;
            }
            //get the unprocessed null records
            $nlists = self::get_unprocessed_null_lists( );
            if ($nlists === 'error') {
                job_log('observation_lists failed 3');
                return;
            }

            //preparing the time conversion sql 
            $time_conversion = "to_timestamp(%s::bigint/1000)";
            $duration_conversion = "($time_conversion - $time_conversion)::time";
            if (isset($only_time) and $only_time) {
                $time_conversion .= "::time";
                if (isset($time_as_int) and $time_as_int) {
                    $time_conversion = "extract(epoch from date_trunc('minute',$time_conversion))/60::integer";
                    $duration_conversion = "extract(epoch from date_trunc('minute',$duration_conversion))/60::integer";
                }
            }

            $warnings = [];
            $lists = array_merge($lists, $nlists);
            foreach ($lists as $list) {
                #job_log("Processing observation session: ".$list['observation_list_id']);
                
                $cmd = [];
                $update_cmd = [];
                $list_id_column = $list_id_column ?? 'obm_observation_list_id';

                if ($list['measurements_num'] != count(explode(',',$list['observation_list_elements']))) { //2 adat
                    // Itt eldobjuk a nullrekordok sorait!
                    continue;
                }

                $update_cmd[] = sprintf("%s = %s", $list_id_column, quote($list['observation_list_id']));
                if (isset($list_start_column)) {
                    if (!is_null($list['observation_list_start'])) {
                        $update_cmd[] = sprintf("%s = $time_conversion", $list_start_column, $list['observation_list_start']);
                    }
                    else {
                        $warnings[] = [
                            'observation_list_id' => $list['observation_list_id'],
                            'message' => 'observation_list_start missing'
                        ];
                    }
                }
                if (isset($list_end_column)) {
                    if (!is_null($list['observation_list_end'])) {
                        $update_cmd[] = sprintf("%s = $time_conversion", $list_end_column, $list['observation_list_end']);
                    }
                    else {
                        $warnings[] = [
                            'observation_list_id' => $list['observation_list_id'],
                            'message' => 'observation_list_end missing'
                        ];
                    }
                }
                if (isset($list_duration_column)) {
                    $cmd2 = sprintf("SELECT FROM public.%2\$s WHERE obm_uploading_id IN (%3\$s) AND %1\$s IS NOT NULL;", $list_duration_column, $table, $list['observation_list_elements']);
                    if (! $res = pg_query($ID, $cmd2)) {
                        job_log("$table query error");
                        return;
                    }
                    if (! pg_num_rows($res) && !is_null($list['observation_list_end']) && !is_null($list['observation_list_start'])) {
                        $update_cmd[] = sprintf("%s = $duration_conversion", $list_duration_column, $list['observation_list_end'], $list['observation_list_start']);
                    }
                }

                $cmd[] = sprintf("UPDATE public.%s SET %s WHERE obm_uploading_id IN (%s) AND %s IS NULL;", 
                            $table, 
                            implode(', ',$update_cmd), 
                            $list['observation_list_elements'], 
                            $list_id_column );

                $cmd[] = sprintf("INSERT INTO %s_observation_sessions 
                            (sids, uploading_id, uploader_id, session_start, session_end, nullrecord) 
                            VALUES (%s, %s, %s, %s, %s, %s);",
                            PROJECTTABLE,
                            quote($list['observation_list_id']),
                            quote($list['uploading_id']),
                            quote($list['uploader_id']),
                            (is_null($list['observation_list_start'])) ? 'NULL' : "to_timestamp({$list['observation_list_start']}::bigint/1000)",
                            (is_null($list['observation_list_end'])) ? 'NULL' : "to_timestamp({$list['observation_list_end']}::bigint/1000)",
                            (is_null($list['observation_list_null_record'])) ? 'NULL' : $list['observation_list_null_record']
                );

                #job_log($list['observation_list_id'] .": ". implode($cmd));
                if ( query($ID, $cmd) ) {
                    #job_log($list['observation_list_id'] . ': list processed');
                } else {
                    job_log($list['observation_list_id'] . ': list NOT processed');
                    job_log($cmd);
                }
            }
            
        }
    }

    public static function get_unprocessed_lists() {
        global $ID;
        $cmd = sprintf("WITH upl AS (SELECT * FROM system.uploadings WHERE
                            project = '%1\$s' AND
                            project_table NOT LIKE 'temporary_tables.%2\$s')
                        SELECT
                            l.id AS uploading_id,
                            l.uploader_id,
                            l.uploader_name,
                            l.uploading_date,
                            l.metadata->>'observation_list_id' as observation_list_id,
                            l.metadata->>'observation_list_start' as observation_list_start,
                            l.metadata->>'observation_list_end' as observation_list_end,
                            l.metadata->>'observation_list_null_record' as observation_list_null_record,
                            l.metadata->>'measurements_num' as measurements_num,
                            string_agg(o.id::text,',') as observation_list_elements
                        FROM upl l LEFT JOIN upl o ON l.metadata->>'observation_list_id' = o.metadata->>'observation_list_id'
                        WHERE
                            l.metadata::jsonb ? 'measurements_num' AND
                            NOT o.metadata::jsonb ? 'measurements_num'
                        GROUP BY l.id, observation_list_id, observation_list_start, observation_list_end, observation_list_null_record, measurements_num, l.uploader_id, l.uploader_name, l.uploading_date
                        ORDER BY uploading_id;",
                        PROJECTTABLE,
                        '%'
                    );
        if (! $res = query($ID, $cmd) ) {
            return 'error';
        }
        $results = pg_fetch_all($res[0]);
        return ($results) ? $results : [];
    }
    
    public static function get_unprocessed_null_lists() {
        global $ID;
        $cmd = sprintf("WITH upl AS (SELECT * FROM system.uploadings WHERE
                            project = '%1\$s' AND
                            project_table NOT LIKE 'temporary_tables.%2\$s')
                        SELECT
                            l.id AS uploading_id,
                            l.uploader_id,
                            l.uploader_name,
                            l.uploading_date,
                            l.metadata->>'observation_list_id' as observation_list_id,
                            l.metadata->>'observation_list_start' as observation_list_start,
                            l.metadata->>'observation_list_end' as observation_list_end,
                            l.metadata->>'observation_list_null_record' as observation_list_null_record,
                            l.metadata->>'measurements_num' as measurements_num,
                            l.id::text as observation_list_elements
                        FROM upl l 
                        WHERE
                            l.metadata::jsonb ? 'observation_list_null_record' AND
                            l.metadata->>'observation_list_null_record' = 'true'
                        GROUP BY l.id, observation_list_id, observation_list_start, observation_list_end, observation_list_null_record, measurements_num, l.uploader_id, l.uploader_name, l.uploading_date
                        ORDER BY uploading_id;",
                        PROJECTTABLE,
                        '%'
                    );
        if (! $res = query($ID, $cmd) ) {
            return 'error';
        }
        $results = pg_fetch_all($res[0]);
        return ($results) ? $results : [];
    }

}
?>
