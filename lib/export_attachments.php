<?php
class export_attachments extends job_module {

    public function __construct($mtable) {
        parent::__construct(__CLASS__,$mtable);
    }

    public function init($params,$pa) {
        debug('export_attachments initialized', __FILE__, __LINE__);
        return true;
    }

    static function run($run_opts) {
        job_log("running export_attachments module");
        global $ID;

        $params = parent::getJobParams(__CLASS__);

        #if (!$params) {
        #    job_log('use parameters: {\n"sleep": "60",\n"parameter2": "value"\n}');
        
        if ($params) {
            job_log($params);
            extract((array)$params);
        }

        if ($run_opts!='') {
            $j = json_decode(base64_decode($run_opts),true);
            extract((array)$j);
        }
        if (!isset($protocol)) $protocol = 'http';
        
        $path = OB_ROOT.'projects/'.PROJECTTABLE.'/';
        if (!file_exists($path.'attached_files')) {
            job_log("Dir not exists: $path".'attached_files');
            return;
        }
        if (!file_exists($path.'attached_files/exports/')) {
            mkdir($path.'attached_files/exports/');
        }
        $jobhash = hash('ripemd160', MyHASH);
        $htaccess = '
SetEnvIfExpr "%{QUERY_STRING} =~ /^'.$jobhash.'$/" let_me_in            
<FilesMatch ^>
    Order Deny,Allow
    Deny from all
    Allow from env=let_me_in
</FilesMatch>';
        // regenerate .htaccess files allways
        file_put_contents($path.'attached_files/exports/.htaccess', $htaccess);

        $dir = opendir($path.'attached_files');
        $files = array();
        while (false !== ($fname = readdir($dir)))
        {
            if (is_file($path.'attached_files/'.$fname))
            {
                $files[] = $fname;
            }
        }

        $DATATABLE = isset($table) ? $table : PROJECTTABLE;

        $archive = 'attached_files_'.$DATATABLE.'.tar';

        if (file_exists($path.'attached_files/exports/'.$archive))
            unlink($path.'attached_files/exports/'.$archive);

        $a = new PharData($path.'attached_files/exports/'.$archive);
        $tarfile = $path.'attached_files/exports/'.$archive;

        $cmd = sprintf('SELECT f.data_table,array_to_string(array_agg(DISTINCT fc.conid),\',\') AS conid,f.id,f.reference,f.comment,array_to_string(array_agg(k.%2$s),\',\') as obm_id,f.datum
                    FROM system.files f
                    LEFT JOIN system.file_connect fc ON f.id=fc.file_id
                    LEFT JOIN %4$s k ON (k.obm_files_id=fc.conid)
                    WHERE f.project_table=\'%3$s\' AND (k.obm_files_id IS NOT NULL OR
                           (k.obm_files_id IS NULL AND f.data_table=%1$s) )
                    GROUP BY f.id,f.reference,f.comment
                    ORDER BY f.datum',quote($DATATABLE),'obm_id',PROJECTTABLE,$DATATABLE);
        

        $res = pg_query($ID,$cmd);
        
        //$workers[] = exec('php download_attachments.php '.$task.' &  echo $!');

        while ($row = pg_fetch_assoc($res)) {

            $key = array_search($row['reference'],$files);
            if ($key!==false) {    
                $fname = $files[$key];
                $conids = preg_split('/,/',$row['conid']);
                foreach($conids as $conid) {
                    $valid_filename = preg_replace('/[*]/','_',$conid.'_'.$fname);
                    $a->addFile($path.'attached_files/'.$fname,$valid_filename);
                }
            }
        }

        job_log("Download <a href='$protocol://".URL."/attached_files/exports/$archive?$jobhash' target='_blank'>$archive</a>");

    }
}
?>
