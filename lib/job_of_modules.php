<?php
class job_of_modules extends job_module {

    public function __construct($mtable) {
        parent::__construct(__CLASS__,$mtable);
    }

    public function init($params,$pa) {
        debug('job_of_modules initialized', __FILE__, __LINE__);
        return true;
    }

    static function run() {
        $params = parent::getJobParams(__CLASS__);
        
        $modules = new modules(PROJECTTABLE);
        
        foreach ($modules->which_has_method('job_run', PROJECTTABLE) as $m) {
            if (isset($params->$m)) {
                $schedule = $params->$m;
                $schedule[0] = roundUp($schedule[0]);
                $now = [ roundUp(date('i')), date('H'), date('N') ];
                for ($i=0; $i < 3; $i++) {
                    if ($schedule[$i] != $now[$i] && $schedule[$i] != '*') {
                        return;
                    }
                }
            }
            $modules->_include($m, 'job_run');
        }

    }
}
?>
