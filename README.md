# web-app jobs

OpenBioMaps web-app jobs

Create a cron entry to put the jobs to work:

`*/5 * * * * /usr/bin/php /var/www/html/biomaps/projects/YOUR-PROJECT/jobs.php &> /dev/null`


The php jobs typically has two files with the same name which should be placed into jobs/run/ and jobs/run/lib/ directories.

Other types can be placed only into the 'run' directory.